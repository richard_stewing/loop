-module (infintyLoop).
-export ([start/0]).

start()->
	loop(0),
	ok.

loop(N)->
	if
		N < 10000000 ->
			loop(N+1);
		N >= 10000000 ->
		io:format("you got throw~n"),
		start()
	end
.